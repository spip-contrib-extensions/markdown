<?php
/**
 * Fonctions utiles au plugin Markdown
 *
 * @plugin     Markdown
 * @copyright  2014
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Markdown\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

// desactiver l'insertion auto par le porte plume pour piloter via les classes
define('_PORTE_PLUME_INSERER_AUTO_NAME_TEXTE', false);

// s'inserer a la fin de pre_propre et post propre
$GLOBALS['spip_pipeline']['pre_propre'] = (isset($GLOBALS['spip_pipeline']['pre_propre'])?$GLOBALS['spip_pipeline']['pre_propre']:'').'||markdown_pre_propre';
$GLOBALS['spip_pipeline']['post_propre'] = (isset($GLOBALS['spip_pipeline']['post_propre'])?$GLOBALS['spip_pipeline']['post_propre']:'').'||markdown_post_propre';
$GLOBALS['spip_pipeline']['post_typo'] = (isset($GLOBALS['spip_pipeline']['post_propre'])?$GLOBALS['spip_pipeline']['post_propre']:'').'||markdown_post_typo';
$GLOBALS['spip_pipeline']['formulaire_fond'] = (isset($GLOBALS['spip_pipeline']['formulaire_fond'])?$GLOBALS['spip_pipeline']['formulaire_fond']:'').'||markdown_formulaire_fond';


/**
 * Post typo : retablir les blocs de code dans le MarkDown
 * qui ont ete echappes en pre-liens
 * La on retrouve tout le contenu MarkDown initial, qui a beneficie des corrections typo
 * @param string $texte
 * @return string
 */
function markdown_post_typo($texte){
	if (strpos($texte,"<md>")!==false){
		$texte = echappe_retour($texte,"md");
	}
	return $texte;
}


/**
 * Pre-propre : traiter les raccourcis markdown
 * @param string $texte
 * @return string
 */
function markdown_pre_propre($texte){
	if (!function_exists('markdown_filtre_portions_md')) {
		include_spip('inc/markdown');
	}

	$mes_notes = "";
	// traiter les notes ici si il y a du <md> pour avoir une numerotation coherente
	if (strpos($texte,"<md>")!==false
	  AND strpos($texte,"[[")!==false){
		$notes = charger_fonction('notes', 'inc');
		// Gerer les notes (ne passe pas dans le pipeline)
		list($texte, $mes_notes) = $notes($texte);
	}

	$texte = markdown_filtre_portions_md($texte,"markdown_raccourcis");

	if ($mes_notes)
		$notes($mes_notes,'traiter');

	return $texte;
}


/**
 * @param $texte
 * @return string
 */
function markdown_post_propre($texte){
	static $hreplace=null;
	static $hmini=null;
	if (is_null($hreplace)){
		$hreplace = false;
		// on peut forcer par define, utile pour les tests unitaires
		if (defined('_MARKDOWN_HMINI'))
			$hmini = _MARKDOWN_HMINI;
		else {
			include_spip('inc/config');
			$hmini = lire_config("markdown/hmini",1);
		}
		if ($hmini>1){
			$hreplace = array();
			for ($i=5;$i>=1;$i--){
				$ir = min($i+1,6);
				$hreplace[1]["<h$i"] = "<h$ir";
				$hreplace[1]["</h$i"] = "</h$ir";
				$ir = min($i+2,6);
				$hreplace[2]["<h$i"] = "<h$ir";
				$hreplace[2]["</h$i"] = "</h$ir";
			}
		}
	}

	// blocs <md></md> echappes
	if (strpos($texte,'<div class="base64md')!==false){
		$texte = echappe_retour($texte,"md");
	}


	// la globale $GLOBALS['markdown_inh_hreplace'] permet d'inhiber le replace
	// utilisee dans le titrage automatique
	if (!isset($GLOBALS['markdown_inh_hreplace'])
		AND $hreplace AND strpos($texte,"</h")!==false){
		// si on veut h3 au plus haut et qu'il y a des h1, on decale de 2 vers le bas
		if ($hmini==3 AND strpos($texte,"</h1")){
			$texte = str_replace(array_keys($hreplace[2]),array_values($hreplace[2]),$texte);
		}
		// sinon si on veut h2 et qu'il y a h1 ou si on veut h3 et qu'il y a h2, on decale de 1 vers le bas
		elseif ( ($hmini==2 AND strpos($texte,"</h1"))
			OR ($hmini==3 AND strpos($texte,"</h2")) ){
			$texte = str_replace(array_keys($hreplace[1]),array_values($hreplace[1]),$texte);
		}
	}

	return $texte;
}

/**
 * Formulaire fond : inserer a la volee les classes adhoc sur les textarea name='texte'
 * selon la syntaxe par defaut
 *
 * @param array $flux
 * @return array
 */
function markdown_formulaire_fond($flux) {

	// check rapide pour voir si on a un textarea et un name=texte
	if (stripos($flux['data'], '<textarea')
	  and (stripos($flux['data'], 'name="texte"') or stripos($flux['data'], "name='texte'")or stripos($flux['data'], "name=texte"))) {

		// ok on peut utiliser les fonctions lentes
		if (preg_match_all(",<textarea\b[^>]*>,Uims", $flux['data'], $matches)) {
			if (!function_exists('markdown_syntaxe_defaut')){
				include_spip('inc/markdown');
			}
			$classe_prefixe = (markdown_syntaxe_defaut() === 'spip' ? 'inserer' : 'inserer_md');

			$classe_textarea = '';
			if ($flux['args']['form'] === 'forum') {
				if (!empty($GLOBALS['meta']['forums_afficher_barre']) and $GLOBALS['meta']['forums_afficher_barre'] !== 'non') {
					$classe_textarea = $classe_prefixe . '_barre_forum';
				}
			}
			else {
				$classe_textarea = "{$classe_prefixe}_barre_edition {$classe_prefixe}_previsualisation";
			}
			if (!function_exists('extraire_attribut')) {
				include_spip('inc/filtre');
			}
			if ($classe_textarea) {
				foreach ($matches[0] as $textarea) {
					$name = extraire_attribut($textarea, 'name');
					if ($name == 'texte') {
						$class = extraire_attribut($textarea, 'class');
						$class = trim($class . " " . $classe_textarea);
						$tmod = inserer_attribut($textarea, 'class', $class);
						$flux['data'] = str_replace($textarea, $tmod, $flux['data']);
						// break ? peut-on avoir 2 textarea name="texte" dans un seul formulaire ? tordu mais oui..
					}
				}
			}
		}

	}

	return $flux;
}