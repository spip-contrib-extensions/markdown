<?php
/**
 * Fonctions utiles au plugin Markdown
 *
 * @plugin     Markdown
 * @copyright  2014
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Markdown\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Parser du markdown
 *
 * @param string $texte
 * @return string
 */
function markdown_propre($texte){
	if (!function_exists('markdown_syntaxe_defaut')) {
		include_spip('inc/markdown');
	}
	if (markdown_syntaxe_defaut()==="spip"){
		$texte = "<md>$texte</md>";
	}

	return propre($texte);
}


/**
 * Concertir un texte en syntaxe 'markdown par defaut' en texte en syntaxe 'spip par defaut'
 *
 * @param string $texte
 * @return string
 */
function markdown_contenu_md_to_spip($texte) {

	if (strpos($texte, '<spip>') !== false) {
		$texte = str_replace(array("<spip>","</spip>"),array("</md>","<md>"),$texte);
	}
	$texte = "<md>$texte</md>";
	if (strpos($texte, '<md></md>') !== false){
		$texte = str_replace("<md></md>", "", $texte);
	}

	return $texte;
}

/**
 * Concertir un texte en syntaxe 'spip par defaut' en texte en syntaxe 'markdown par defaut'
 *
 * @param string $texte
 * @return string
 */
function markdown_contenu_spip_to_md($texte) {

	if (strpos($texte, '<md>') !== false) {
		$texte = str_replace(array("<md>","</md>"),array("</spip>","<spip>"),$texte);
	}

	$texte = "<spip>$texte</spip>";
	if (strpos($texte, '<spip></spip>') !== false){
		$texte = str_replace("<spip></spip>", "", $texte);
	}

	return $texte;
}


/**
 * Avant tout echappement pour propre, on peut preparer le contenu
 * @param string $texte
 * @return string
 */
function markdown_pre_echappe_html_propre($texte){
	if (!function_exists('markdown_syntaxe_defaut')) {
		include_spip('inc/markdown');
	}

	// si syntaxe par defaut est markdown et pas de <md> dans le texte on les introduits
	if (markdown_syntaxe_defaut()==="markdown"
		// est-ce judicieux de tester cette condition ?
	  AND strpos($texte,"<md>")===false
	  ){
		$texte = markdown_contenu_md_to_spip($texte);
	}

	if (strpos($texte,"<md>")!==false){
		$texte = echappe_html($texte,"mdblocs",false,',<(md)(\s[^>]*)?'.'>(.*)</\1>,UimsS');
	}


	return $texte;
}

/**
 * Avant le traitemept typo et liens :
 * - des-echapper les blocs <md> qui ont ete echappes au tout debut
 *
 * @param string $texte
 * @return string
 */
function markdown_pre_liens($texte){
	// si pas de base64 dans le texte, rien a faire
	if (strpos($texte,"base64mdblocs")!==false) {
		// il suffit de desechapper les blocs <md> (mais dont on a echappe le code)
		$texte = echappe_retour($texte,'mdblocs');
	}

	// ici on a le html du code SPIP echappe, mais sans avoir touche au code MD qui est echappe aussi
	return $texte;
}


/**
 * Pre typo : Rien a faire on dirait
 * @param string $texte
 * @return string
 */
function markdown_pre_typo($texte){
	return $texte;
}


/**
 * Determiner un titre automatique,
 * a partir des champs textes de contenu
 *
 * @param array $champs_contenu
 *   liste des champs contenu textuels
 * @param array|null $c
 *   tableau qui contient les valeurs des champs de contenu
 *   si null on utilise les valeurs du POST
 * @param int $longueur
 *   longueur de coupe
 * @return string
 */
function inc_titrer_contenu($champs_contenu, $c=null, $longueur=80){
	// prendre la concatenation des champs texte
	$t = "";
	foreach($champs_contenu as $champ){
		$t .= _request($champ,$c)."\n\n";
	}

	if ($t){
		$GLOBALS['markdown_inh_hreplace'] = true;
		include_spip("inc/texte");
		$t = propre($t);
		unset($GLOBALS['markdown_inh_hreplace']);
		if (strpos($t,"</h1>")!==false
		  AND preg_match(",<h1[^>]*>(.*)</h1>,Uims",$t,$m)){
			$t = $m[1];
		}
		else {
			$t = couper($t,$longueur,"...");
		}
	}

	return $t;
}
